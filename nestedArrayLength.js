// https://edabit.com/challenge/yXSTvCNen2DQHyrh6

const lengthOfNestedArray = function (array) {

	let length = 0;

	for (let item of array) {

		if (Array.isArray(item)) {
			length += lengthOfNestedArray(item);
		} else {
			length++;
		}

	}



	return length;

};



// A poniżej nieco inne podejście do problemu.
// Funkcja może krótsza, ładnie wygląda - ale pewne argumenty dają złe wyniki.
// Chodzi o tablicę pustą, tablice zawierające tablice puste i 
// tablice zawierające łańcuchy z przecinkami.

const lengthOfNestedArrayWithoutCommasAndEmptyArrays = function (array) {
	return (array + "").split(",").length;
};