// https://edabit.com/challenge/6R6gReGTGwzpwuffD

const sevenBoom = function (x) {

	for (let element of x) {
		element += "";

		for (let letter of element) {
			if (letter === "7") {
				return "Boom!";
			}
		}


	}

	return "There is no 7 in the array";

};