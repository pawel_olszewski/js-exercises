// https://edabit.com/challenge/s5Sz2ovKsvtGxNGgn

const Book = function (title, author) {
	this.title = title;
	this.author = author;
};

Book.prototype.getTitle = function () {
	console.log("Title: " + this.title);
};

Book.prototype.getAuthor = function () {
	console.log("Author: " + this.author);
};
