// https://edabit.com/challenge/vHvu4Wis8RhmQbwXm

const additivePersistence = function (x) {

	let persistence = 0;

	while (x > 9) {
		x += "";
		let y = 0;
		for (let i = 0; i < x.length; i++) {
			y += parseInt(x[i]);
		}
		x = y;
		persistence++;
	}

	return persistence;

}


//-------

const multiplicativePersistence = function (x) {

	let persistence = 0;

	while (x > 9) {
		x += "";
		let y = 1;
		for (let i = 0; i < x.length; i++) {
			y *= parseInt(x[i]);
		}
		x = y;
		persistence++;
	}

	return persistence;

};