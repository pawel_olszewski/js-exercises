//https://edabit.com/challenge/r6TSNwkLZ2DgsoKiH

const sumDigits = function (x) {

	x += "";
	let sum = 0;

	for (let digit of x) {
		digit = parseInt(digit);

		if (!isNaN(digit)) {
			sum += digit;
		}

	}

	return sum;
	
};


//--------------------

const oddishOrEvenish = function (x) {
	let sum = sumDigits(x);
	if (sum % 2 === 0) {
		return "Evenish";
	}
	return "Oddish";
};