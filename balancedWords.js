// https://edabit.com/challenge/8QTBwLzAdaM8wkrXu

const numberOfLetter = function (x) {
	if (typeof x === "string" && x.length === 1) {
		return x.charCodeAt(0) - 96;
	} 
	return undefined;
};


const balanced = function (x) {

	let part1, part2;

	if (x.length % 2 === 1) {
		part1 = x.slice(0, Math.floor(x.length / 2) )
		part2 = x.slice(Math.ceil(x.length / 2));
	} else {
		part1 = x.slice(0, x.length / 2);
		part2 = x.slice(x.length / 2);
	}


	let part1value = 0, part2value = 0;

	for (let letter of part1) {
		part1value += numberOfLetter(letter)
	}

	for (let letter of part2) {
		part2value += numberOfLetter(letter)
	}

	if (part1value === part2value) {
		return true;
	}
	
	return false;

};