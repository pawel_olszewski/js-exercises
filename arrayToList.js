// Zmiana tablicy w obiekt - listę uporządkowaną
// Zadanie z książki ,,Zrozumieć JavaScript. Wprowadzenie do programowania"


const arrayToList = function (array) {

	let list = {next: null};

	for (let i = array.length - 1; i >= 0; i--) {

		list.value = array[i]

		if (i !== 0) {
			list = {next: list};
		}

	}

	return list;

};