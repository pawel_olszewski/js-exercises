// https://edabit.com/challenge/8p7apuCwgSzWkaTC8

const singleSquish = function (array, direction) {

	if (array.length < 2) {
		return array;
	}

	let result = [];



	if (direction === "left" || direction === undefined) {

		result.push(array[0] + array[1]);

		for (let i = 2; i < array.length; i++) {
			result.push(array[i])
		}

	} else {

		result.push(array[array.length - 1] + array[array.length - 2]);

		for (let i = array.length - 3; i >= 0; i--) {
			result.unshift(array[i])
		}


	}
	

	return result;

};



//--------

const squish = function (array, direction) {

	if (array.length < 2) {
		return array;
	}

	let result = [array];

	let finalLength = array.length;


	while (result.length < finalLength) {
		array = singleSquish(array, direction);
		result.push(array);
	}

	return result;

};